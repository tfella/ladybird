// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: 2022 Julius Künzel <jk.kdedev@smartlab.uber.space>

#include <QApplication>
#include <QQmlApplicationEngine>
#include <QUrl>
#include <QtQml>
#include <QQuickStyle>
#include <QQmlContext>
#include <QQuickView>

#include "app.h"
#include "version-ladybird.h"
#include <KAboutData>
#include <KLocalizedContext>
#include <KLocalizedString>

#include "database.h"
#include "ladybirdconfig.h"
#include "bugmodel.h"
#include "bugsproxymodel.h"
#include "templatesmodel.h"

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication app(argc, argv);

#ifdef Q_OS_ANDROID
    QQuickStyle::setStyle(QStringLiteral("Material"));
#else
    if (qEnvironmentVariableIsEmpty("QT_QUICK_CONTROLS_STYLE")) {
        QQuickStyle::setStyle(QStringLiteral("org.kde.desktop"));
    }
#endif

#ifdef Q_OS_WINDOWS
    if (AttachConsole(ATTACH_PARENT_PROCESS)) {
        freopen("CONOUT$", "w", stdout);
        freopen("CONOUT$", "w", stderr);
    }

    QApplication::setStyle(QStringLiteral("breeze"));
    auto font = app.font();
    font.setPointSize(10);
    app.setFont(font);
#endif

    QCoreApplication::setOrganizationName(QStringLiteral("KDE"));
    QCoreApplication::setApplicationName(QStringLiteral("Ladybird"));

    KAboutData aboutData(
        // The program name used internally.
        QStringLiteral("ladybird"),
        // A displayable program name string.
        i18nc("@title", "Ladybird"),
        // The program version string.
        QStringLiteral(LADYBIRD_VERSION_STRING),
        // Short description of what the app does.
        i18n("A bugzilla client"),
        // The license this code is released under.
        KAboutLicense::GPL_V3,
        // Copyright Statement.
        i18n("© 2023 KDE Community"));
    aboutData.addAuthor(i18nc("@info:credit", "Julius Künzel"), i18nc("@info:credit", "Developer"), QStringLiteral("jk.kdedev@smartlab.uber.space"), QStringLiteral("https://my.kde.org/user/jlskuz/"));
    KAboutData::setApplicationData(aboutData);

    QQmlApplicationEngine engine;

    auto config = LadybirdConfig::self();

    qmlRegisterType<BugModel>("org.kde.ladybird", 1, 0, "BugModel");
    qmlRegisterType<BugsProxyModel>("org.kde.ladybird", 1, 0, "BugsProxyModel");

    qmlRegisterType<TemplatesModel>("org.kde.ladybird", 1, 0, "TemplatesModel");


    qmlRegisterSingletonInstance("org.kde.ladybird", 1, 0, "Config", config);
    QObject::connect(&app, &QCoreApplication::aboutToQuit, config, &LadybirdConfig::save);

    qmlRegisterSingletonType("org.kde.ladybird", 1, 0, "About", [](QQmlEngine *engine, QJSEngine *) -> QJSValue {
        return engine->toScriptValue(KAboutData::applicationData());
    });

    App application;
    qmlRegisterSingletonInstance("org.kde.ladybird", 1, 0, "App", &application);

    qmlRegisterSingletonInstance("org.kde.ladybird", 1, 0, "Database", &Database::instance());

    Database::instance();

    engine.rootContext()->setContextObject(new KLocalizedContext(&engine));
    engine.load(QUrl(QStringLiteral("qrc:///main.qml")));

    if (engine.rootObjects().isEmpty()) {
        return -1;
    }

    return app.exec();
}
