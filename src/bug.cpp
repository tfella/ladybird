// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: 2022 Julius Künzel <jk.kdedev@smartlab.uber.space>

#include "bug.h"

#include <QApplication>
#include <QClipboard>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QNetworkReply>
#include <QNetworkRequest>

#include "database.h"
#include "ladybirdconfig.h"

Bug::Bug(const QJsonObject &json)
    : QObject(nullptr)
    , m_id(QString::number(json[QStringLiteral("id")].toInt()))
    , m_status(json[QStringLiteral("status")].toString())
    , m_summary(json[QStringLiteral("summary")].toString())
    , m_severity(json[QStringLiteral("severity")].toString())
    , m_platform(json[QStringLiteral("platform")].toString())
    , m_os(json[QStringLiteral("op_sys")].toString())
    , m_version(json[QStringLiteral("version")].toString())
    , m_lastUpdate(QDateTime::fromString(json[QStringLiteral("last_change_time")].toString(), Qt::ISODate))
    , m_creationTime(QDateTime::fromString(json[QStringLiteral("creation_time")].toString(), Qt::ISODate))
    , m_commentDraft()
    , m_statusDraft(0)
    , m_seen(false)
    , m_errorString(QLatin1String(""))

{
    QJsonArray flags = json[QStringLiteral("flags")].toArray();
    for (auto flag : flags) {
        m_flags.append(flag.toObject()[QStringLiteral("name")].toString());
    }
    QSqlQuery bugQuery;
    bugQuery.prepare(QStringLiteral("SELECT * FROM Bugs WHERE id=:id"));
    bugQuery.bindValue(QStringLiteral(":id"), m_id);
    Database::instance().execute(bugQuery);
    if (!bugQuery.next()) {
        bugQuery.clear();
        bugQuery.prepare(QStringLiteral("INSERT OR IGNORE INTO Bugs VALUES (:id, false);"));
        bugQuery.bindValue(QStringLiteral(":id"), m_id);
        Database::instance().execute(bugQuery);
    } else {
        m_seen = bugQuery.value(QStringLiteral("seen")).toBool();
    }
}

Bug::~Bug()
{
    qDeleteAll(m_comments);
}

void Bug::fetchComments(bool force)
{
    if (!m_comments.isEmpty() && !force) {
        return;
    }
    m_comments.clear();
    Q_EMIT commentsChanged(m_comments);
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    manager->setRedirectPolicy(QNetworkRequest::NoLessSafeRedirectPolicy);
    manager->setStrictTransportSecurityEnabled(true);
    manager->enableStrictTransportSecurityStore(true);

    // https://bugzilla.readthedocs.io/en/5.0/api/core/v1/bug.html#search-bugs
    QString url = QStringLiteral("%1/rest/bug/%2/comment").arg(LadybirdConfig::baseUrl()).arg(m_id);
    QNetworkRequest request((QUrl(url)));
    QNetworkReply *reply = manager->get(request);
    connect(reply, &QNetworkReply::finished, this, [this, url, reply]() {
        //setRefreshing(false);
        if (reply->error()) {
            qWarning() << "Error fetching feed";
            m_errorString = reply->errorString();
            Q_EMIT errorStringChanged(m_errorString);
            qWarning() << reply->errorString();
            //Q_EMIT error(url, reply->error(), reply->errorString());
        } else {
            QByteArray data = reply->readAll();
            processCommentResult(data);
        }
        delete reply;
    });
}

void Bug::processCommentResult(QByteArray &data)
{
    qDebug() << __FUNCTION__ ;
    QJsonDocument doc = QJsonDocument::fromJson(data);
    QJsonArray array = doc[QStringLiteral("bugs")].toObject()[m_id].toObject()[QStringLiteral("comments")].toArray();

    qDeleteAll(m_comments);
    m_comments.clear();

    for (auto item : array) {
        m_comments << new Comment(item.toObject());
    }
    Q_EMIT commentsChanged(m_comments);
}

void Bug::setSeen(bool seen) {
    m_seen = seen;
    Q_EMIT seenChanged(seen);
    Database::instance().setSeen(m_id, m_seen);
}

void Bug::setCommentDraft(const QString &commentDraft) {
    m_commentDraft = commentDraft;
}

void Bug::setStatusDraft(int statusDraft) {
    m_statusDraft = statusDraft;
}

void Bug::copyId()
{
    QClipboard *clipboard = QGuiApplication::clipboard();
    clipboard->setText(m_id);
}

void Bug::postComment(const QString &comment, int status, QString dupId)
{
    int dupIdInt = dupId.toInt();

    if (status == 8 && (dupId.isEmpty() || dupIdInt == 0)) {
        qWarning() << "invalid duplicate ID" << dupId << dupIdInt;
        return;
    }

    QNetworkAccessManager *manager = new QNetworkAccessManager(this);

    QString url = QStringLiteral("%1/rest/bug/%2").arg(LadybirdConfig::baseUrl()).arg(m_id);
    QNetworkRequest request((QUrl(url)));
    request.setHeader(QNetworkRequest::ContentTypeHeader, QStringLiteral("application/json"));

    QJsonObject obj;
    QJsonObject commentObj;
    commentObj[QStringLiteral("body")] = comment;
    obj[QStringLiteral("comment")] = commentObj;

    switch (status) {
        case 1: // needs info
            obj[QStringLiteral("status")] = QStringLiteral("NEEDSINFO");
            obj[QStringLiteral("resolution")] = QStringLiteral("WAITINGFORINFO");
            break;
        case 2: // fixed
            obj[QStringLiteral("status")] = QStringLiteral("RESOLVED");
            obj[QStringLiteral("resolution")] = QStringLiteral("FIXED");
            break;
        case 3: // unmaintained
            obj[QStringLiteral("status")] = QStringLiteral("RESOLVED");
            obj[QStringLiteral("resolution")] = QStringLiteral("UNMAINTAINED");
            break;
        case 4: // works for me
            obj[QStringLiteral("status")] = QStringLiteral("RESOLVED");
            obj[QStringLiteral("resolution")] = QStringLiteral("WORKSFORME");
            break;
        case 5: // confirmed
            obj[QStringLiteral("status")] = QStringLiteral("CONFIRMED");
            break;
        case 6: // reported
            obj[QStringLiteral("status")] = QStringLiteral("UNCONFIRMED");
            break;
        case 7: // not a bug
            obj[QStringLiteral("status")] = QStringLiteral("RESOLVED");
            obj[QStringLiteral("resolution")] = QStringLiteral("NOT A BUG");
            break;
        case 8: // duplicate
            obj[QStringLiteral("dupe_of")] = dupIdInt;
            break;
    }

    obj[QStringLiteral("api_key")] = LadybirdConfig::apiKey();

    QJsonDocument doc(obj);
    QByteArray data = doc.toJson();
    qDebug() << obj;

    QNetworkReply *reply = manager->put(request, data);

    QObject::connect(reply, &QNetworkReply::finished, [=](){
        if(reply->error() == QNetworkReply::NoError){
            QByteArray data = reply->readAll();
            update(data);
            fetchComments(true);
            setSeen(true);
        }
        else{
            QString err = reply->errorString();
            qDebug() << err;
        }
        reply->deleteLater();
    });
}

void Bug::update(QByteArray &data)
{
    qDebug() << "Update bug";
    // "{\"bugs\":[{\"id\":459260,\"changes\":{\"is_confirmed\":{\"removed\":\"0\",\"added\":\"1\"},\"status\":{\"added\":\"CONFIRMED\",\"removed\":\"UNCONFIRMED\"}},\"alias\":[],\"last_change_time\":\"2022-09-17T10:27:40Z\"}]}"
    QJsonDocument doc = QJsonDocument::fromJson(data);
    QJsonArray array = doc[QStringLiteral("bugs")].toArray();
    qDebug() << array.size() << "bugs changed";
    for (auto ref : array) {
        auto obj = ref.toObject();
        QString id = QString::number(obj[QStringLiteral("id")].toInt());
        if (id != m_id) {
            qDebug() << id << "does not match" << m_id;
            continue;
        }
        qDebug() << "processing" << m_id;
        QJsonObject changes = obj[QStringLiteral("changes")].toObject();
        qDebug() << changes;
        if (changes.contains(QStringLiteral("status"))) {
            m_status = changes[QStringLiteral("status")].toObject()[QStringLiteral("added")].toString(m_status);
            Q_EMIT statusChanged(m_status);
        }
        /*if (changes.contains(QStringLiteral("resolution"))) {
            m_resolution = changes[QStringLiteral("resolution")].toObject()[QStringLiteral("added")].toString(m_resolution);
            Q_EMIT resolutionChanged(m_resolution);
        }*/
    }
}
