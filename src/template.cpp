// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: 2022 Julius Künzel <jk.kdedev@smartlab.uber.space>

#include "template.h"
#include "database.h"

#include <KMacroExpander>
#include <QHash>

Template::Template(const QString &id, const QString &title, const QString &content, QObject *parent)
    : QObject(parent)
    , m_id(id)
    , m_title(title)
    , m_content(content)
{

}

QString Template::expandedContent(Bug *bug)
{
    QHash<QString, QString> map;
    //map.insert(QStringLiteral("author"), bug);
    map.insert(QStringLiteral("version"), bug->version());
    map.insert(QStringLiteral("status"), bug->status());
    map.insert(QStringLiteral("os"), bug->os());
    map.insert(QStringLiteral("platform"), bug->platform());
    ;
    return KMacroExpander::expandMacros(m_content, map);
}

void Template::update(const QString &title, const QString &content)
{
    if (m_title == title && m_content == content) {
        return;
    }

    if (!Database::instance().updateTemplate(m_id, title, content)) {
        return;
    }

    m_title = title;
    m_content = content;

    Q_EMIT titleChanged(m_title);
    Q_EMIT contentChanged(m_content);
}
