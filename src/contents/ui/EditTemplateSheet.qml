// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: 2022 Julius Künzel <jk.kdedev@smartlab.uber.space>

import QtQuick 2.14
import QtQuick.Controls 2.14 as Controls
import QtQuick.Layouts 1.14
import org.kde.kirigami 2.12 as Kirigami
import org.kde.ladybird 1.0 as Ladybird

Kirigami.OverlaySheet {
    id: root

    property var template

    header: Kirigami.Heading {
        text: i18n("Edit Feed")
    }

    //onTemplateChanged: groupCombo.currentIndex = (root.feed !== undefined) ? groupCombo.indexOfValue(root.feed.groupName) : groupCombo.indexOfValue("")

    contentItem: Kirigami.FormLayout {
        Controls.TextField {
            id: title

            text: (root.template !== undefined) ? template.title : ""
            Layout.fillWidth: true
            Kirigami.FormData.label: i18n("Title:")
        }

        Controls.TextArea {
            id: content

            text: (root.template !== undefined) ? template.content : ""
            Layout.fillWidth: true
            Kirigami.FormData.label: i18n("Content:")
        }

    }

    footer: RowLayout {
        Item {
            Layout.fillWidth: true
        }

        Controls.ToolButton {
            enabled: title.length > 0 && content.length > 0
            text: i18n("OK")

            onClicked: {
                if (template) {
                    template.update(title.text, content.text)
                } else {
                    Ladybird.Database.addTemplate(title.text, content.text)
                }
                root.close();
            }
        }

        Controls.ToolButton {
            text: i18n("Cancel")

            onClicked: root.close()
        }
    }
}
