// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: 2022 Julius Künzel <jk.kdedev@smartlab.uber.space>

import QtQuick 2.14
import QtQuick.Controls 2.14 as Controls
import Qt.labs.platform 1.1
import QtQuick.Layouts 1.14

import org.kde.kirigami 2.20 as Kirigami

import org.kde.ladybird 1.0 as Ladybird

Kirigami.ScrollablePage {
    id: page

    title: i18n("%1 Templates", templatesList.count)

    actions.main: Kirigami.Action {
        id: addTemplateAction
        iconName: "document-new"
        text: i18n("Add Template")
        onTriggered: {
            editTemplate.template = undefined
            editTemplate.open()
        }
    }

    contextualActions: [
        Kirigami.Action {
            text: i18n("Export Templates...")
            iconName: "document-export"
            visible: count !== 0
            onTriggered: exportDialog.open()
        },
        Kirigami.Action {
            text: i18n("Import Templates...")
            iconName: "document-import"
            onTriggered: importDialog.open()
        }
    ]

    Kirigami.PlaceholderMessage {
        visible: templatesList.count === 0

        width: Kirigami.Units.gridUnit * 20
        anchors.centerIn: parent

        text: i18n("No templates available")
        helpfulAction: addTemplateAction
    }

    ListView {
        id: templatesList
        visible: count !== 0
        model: templatesModel

        delegate: Kirigami.SwipeListItem {

            Kirigami.BasicListItem {
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                text: model.template.title
                subtitle: model.template.content
                subtitleItem {
                    elide: Text.ElideRight
                    maximumLineCount: 1
                }

                reserveSpaceForIcon: false             
            }

            actions: [
                Kirigami.Action {
                    icon.name: "entry-edit"
                    tooltip: i18n("Edit")
                    onTriggered: {
                        editTemplate.template = model.template
                        editTemplate.open()
                    }
                },
                Kirigami.Action {
                    icon.name: "entry-delete"
                    tooltip: i18n("Delete")
                    onTriggered: Ladybird.Database.deleteTemplate(model.template.id)

                }
            ]
        }

    }

    EditTemplateSheet {
        id: editTemplate
    }

    FileDialog {
        id: importDialog
        title: i18n("Import Feeds")
        folder: StandardPaths.writableLocation(StandardPaths.HomeLocation)
        nameFilters: [i18n("All Files (*)"), i18n("JSON Files (*.json)")]
        onAccepted: templatesModel.importFromFile(file)
    }

    FileDialog {
        id: exportDialog
        title: i18n("Export Templates")
        folder: StandardPaths.writableLocation(StandardPaths.HomeLocation)
        nameFilters: [i18n("JSON Files (*.json)"), i18n("All Files")]
        onAccepted: templatesModel.exportToFile(file)
        fileMode: FileDialog.SaveFile
    }
}
