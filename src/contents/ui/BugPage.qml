// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: 2022 Julius Künzel <jk.kdedev@smartlab.uber.space>

import QtQuick 2.15
import QtQuick.Controls 2.15 as Controls
import QtQuick.Layouts 1.14

import org.kde.kirigami 2.19 as Kirigami

import org.kde.ladybird 1.0 as Ladybird

Kirigami.ScrollablePage {
    id: page

    property QtObject bug

    function statusIcon (name) {
        switch (name.toLowerCase()) {
            case "confirmed":
                return "emblem-mounted"
            case "resolved":
                return "emblem-success"
            case "needsinfo":
                return "emblem-pause"
            case "reopend":
                return "emblem-encrypted-unlocked"
            default:
                return "emblem-question"
        }
    }

    function severityIcon (name) {
        switch (name.toLowerCase()) {
            case "wishlist":
                return "tools-wizard"
            case "crash":
                return "tools-report-bug"
            case "task":
                return "task-reminder"
            default:
                return ""
        }
    }

    function unspecifiedCheck (value) {
        if (value.toLowerCase() === "other") {
            return true
        }
        if (value === "unspecified") {
            return true
        }
        return false
    }

    title: i18n("Bug %1", bug.id)

    Component.onCompleted: bug.fetchComments()

    header: ColumnLayout {
        Layout.fillWidth: true
        Kirigami.Heading {
            text: bug.summary
            wrapMode: Text.WordWrap
            Layout.fillWidth: true
        }
        Flow {
            Layout.fillWidth: true
            spacing: 10
            Kirigami.Chip {
                text: bug.status
                icon.name: statusIcon(bug.status)
                closable: false
            }
            Kirigami.Chip {
                text: i18n("Severity: %1", bug.severity)
                icon.name: severityIcon(bug.severity)
                closable: false
            }
            Kirigami.Chip {
                text: i18n("OS: %1", bug.os)
                icon.name: unspecifiedCheck(bug.os) ? "data-warning" : ""
                closable: false
            }
            Kirigami.Chip {
                text: i18n("Plattform: %1", bug.platform)
                icon.name: unspecifiedCheck(bug.platform) ? "data-warning" : ""
                closable: false
            }

            Kirigami.Chip {
                text: i18n("Version: %1", bug.version)
                icon.name: unspecifiedCheck(bug.version) ? "data-warning" : ""
                closable: false
            }
        }

    }

    Kirigami.LoadingPlaceholder {
        id: loadingPlaceholder
        anchors.centerIn: parent
        visible: commentList.count === 0 && bug.errorString === ""
    }
    Kirigami.PlaceholderMessage {
        visible: commentList.count === 0 && bug.errorString !== ""

        width: Kirigami.Units.gridUnit * 20
        anchors.centerIn: parent

        text: i18n("Error: %1", bug.errorString)
        icon.name: "data-error"
    }

    Kirigami.CardsListView {
        id: commentList
        model: bug.comments
        Layout.fillWidth: true

        delegate: Kirigami.AbstractCard {

            //NOTE: never put a Layout as contentItem as it will cause binding loops
            header: Kirigami.Heading {
                text: i18n("%1 at %2", modelData.creator, modelData.creationTime.toLocaleString(Locale.ShortFormat))
                wrapMode: Text.WordWrap
                level: 2
            }
            contentItem: Item {
                implicitWidth: delegateLayoutOne.implicitWidth
                implicitHeight: delegateLayoutOne.implicitHeight
                ColumnLayout {
                    id: delegateLayoutOne
                    anchors {
                        left: parent.left
                        top: parent.top
                        right: parent.right
                        //IMPORTANT: never put the bottom margin
                    }
                    Kirigami.Icon {
                        visible: modelData.creator === "bug-janitor@kde.org"
                        source: "help-hint"
                    }
                    Text {
                        Layout.fillWidth: true
                        text: modelData.text
                        wrapMode: Text.WordWrap
                        textFormat: Text.MarkdownText
                        onLinkActivated: Qt.openUrlExternally(link)
                        color: Kirigami.Theme.textColor
                    }
                    Controls.Button {
                        visible: modelData.attachmentId !== 0
                        //Layout.alignment: Qt.AlignRight
                        icon.name: "mail-attachment-symbolic"
                        text: i18n("Open attachment")
                        onClicked: Qt.openUrlExternally("https://bugsfiles.kde.org/attachment.cgi?id=" + modelData.attachmentId)
                    }
                }
            }
        }
    }

    footer: Controls.Pane {
        id: pane
        Layout.fillWidth: true

        ColumnLayout {
            width: parent.width
            Text {
                Layout.alignment: Qt.AlignHCenter
                id: keyMissingWarning
                visible: Ladybird.Config.apiKey.length === 0
                width: parent.width
                text: i18n("To be able to post comments you need to specify an api key in the settings")
                color: Kirigami.Theme.negativeTextColor
            }
            RowLayout {
                visible: Ladybird.Config.apiKey.length > 0
                width: parent.width

                Controls.TextArea {
                    id: messageField
                    text: bug.commentDraft
                    Layout.fillWidth: true
                    placeholderText: i18n("Write a comment")
                    wrapMode: Controls.TextArea.Wrap
                    onTextChanged: bug.commentDraft = text
                }

                Controls.Menu {
                    id: templatesMenu
                    title: i18n("Templates")
                    Repeater {
                        model: templatesModel
                        delegate: Controls.MenuItem {
                            text: model.template.title
                            onTriggered: messageField.append(model.template.expandedContent(bug))
                        }
                    }
                }

                Controls.Button {
                    onClicked: templatesMenu.popup()
                    icon.name: "insert-text-symbolic"
                }
            }
            RowLayout {
                width: parent.width
                Layout.alignment: Qt.AlignRight
                visible: Ladybird.Config.apiKey.length > 0

                Controls.ComboBox {
                    id: statusCombo
                    currentIndex: bug.statusDraft
                    model: ListModel {
                        ListElement { text: "(No change)"; data: 0 }
                        ListElement { text: "Needs Info"; data: 1 }
                        ListElement { text: "Fixed"; data: 2 }
                        ListElement { text: "Unmaintained"; data: 3 }
                        ListElement { text: "Works for me"; data: 4 }
                        ListElement { text: "Confirmed"; data: 5 }
                        ListElement { text: "Reported"; data: 6 }
                        ListElement { text: "Not a Bug"; data: 7 }
                        ListElement { text: "Duplicate of"; data: 8 }
                    }
                    textRole: "text"
                    valueRole: "data"
                    Layout.fillWidth: true
                    onCurrentIndexChanged: bug.statusDraft = currentIndex
                }
                Controls.TextField {
                    id: dupId
                    visible: statusCombo.currentValue === 8
                    placeholderText: i18n("ID of the bug this is a duplicate of")
                }

                Controls.Button {
                    id: sendButton
                    text: i18n("Send")
                    icon.name: "document-send"
                    enabled: messageField.length > 0 || (statusCombo.currentValue === 8 && dupId.length > 0)
                    onClicked: {
                        bug.postComment(messageField.text, statusCombo.currentValue, dupId.visible ? dupId.text : "")
                        messageField.clear()
                        bug.commentDraft = ""
                        dupId.clear()
                        statusCombo.currentIndex = 0
                        bug.statusDraft = 0
                    }
                }
            }
        }
    }

    actions {
        main: Kirigami.Action {
            text: i18n("Open in Browser")
            icon.name: "globe"
            onTriggered: Qt.openUrlExternally("https://bugs.kde.org/" + bug.id)
        }
        contextualActions: [
            Kirigami.Action {
                text: bug.seen ? i18n("Mark as unseen") : i18n("Mark as seen")
                icon.name: bug.seen ? "mail-mark-unread" : "mail-mark-read"
                onTriggered: bug.seen = !bug.seen
            },
            Kirigami.Action {
                text: i18n("Reload Comments")
                icon.name: "view-refresh"
                onTriggered: bug.fetchComments(true)
                enabled: !loadingPlaceholder.visible
            },
            Kirigami.Action {
                text: i18n("Copy Bug ID")
                icon.name: "edit-copy"
                onTriggered: bug.copyId()
            }
        ]
    }
}
