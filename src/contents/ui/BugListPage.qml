// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: 2022 Julius Künzel <jk.kdedev@smartlab.uber.space>

import QtQuick 2.14
import QtQuick.Controls 2.14 as Controls
import QtQuick.Layouts 1.14

import org.kde.kirigami 2.20 as Kirigami

import org.kde.ladybird 1.0 as Ladybird

Kirigami.ScrollablePage {
    id: page

    property var flagsArray: [i18n("All")]
    refreshing: root.bugListRefreshing

    title: i18n("%1 Bugs in %2", bugsList.count, Ladybird.Config.product)
    supportsRefreshing: true

    onRefreshingChanged: {
        if (refreshing) {
            root.bugListRefreshing = true
            bugsModel.fetch()
        }
    }

    function updateFlagsForFilter() {
        flagsArray = bugsModel.getFlagsForFilter();
        flagCombo.currentIndex = Math.max(0, flagsArray.indexOf(proxyBugsModel.flagsFilter))
    }

    contextualActions: [
        Kirigami.Action {
            id: refreshBugsAction
            iconName: "view-refresh"
            text: i18n("Refresh")
            onTriggered: root.bugListRefreshing = true
            visible: !Kirigami.Settings.isMobile || bugsList.count === 0
        }
    ]

    actions.main: Kirigami.Action {
        enabled: bugCount > 0
        id: filterAction
        text: i18n("Filter")
        iconName: "dialog-filters"
        onTriggered: { updateFlagsForFilter(); filterSheet.open(); }
    }

    Kirigami.PlaceholderMessage {
        visible: bugsList.count === 0

        width: Kirigami.Units.gridUnit * 20
        anchors.centerIn: parent

        text: if (bugsModel.errorString === "") {
                  if (bugCount === 0) {
                      i18n("No bugs loaded")
                  } else {
                      i18n("No bugs found")
                  }
              } else {
                  i18n("Error: %1", bugsModel.errorString)
              }

        helpfulAction: bugCount === 0 ? refreshBugsAction : filterAction
        icon.name: bugsModel.errorString === "" ? "" : "data-error"
    }

    Kirigami.PromptDialog {
        // Show this dialog only if we can not show the error in the placeholder.
        // This is eg. the case if the feed worked so far, but refreshing fails for some reason
        visible: bugsList.count !== 0 && bugsModel.errorString !== ""
        title: i18n("Error")
        subtitle: i18n("Error: %1", bugsModel.errorString)
    }

    ListView {
        id: bugsList
        visible: count !== 0
        model: proxyBugsModel

        delegate: Kirigami.SwipeListItem {

            contentItem: Kirigami.BasicListItem {
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                text: model.bug.summary
                subtitle: model.bug.id + " · " + model.bug.status//model.entry.updated.toLocaleString(Qt.locale(), Locale.ShortFormat) + (model.entry.authors.length === 0 ? "" : " " + i18nc("by <author(s)>", "by") + " " + model.entry.authors[0].name)
                reserveSpaceForIcon: false
                highlighted: pageStack.currentItem.title === i18n("Bug %1", model.bug.id)

                onClicked: {
                    while(pageStack.depth > 2)
                        pageStack.pop()
                    pageStack.push("qrc:/BugPage.qml", {"bug": model.bug})
                }
            }

            actions: [
                Kirigami.Action {
                    icon.name: model.bug.seen ? "mail-mark-unread" : "mail-mark-read"
                    tooltip: model.bug.seen ? i18n("Mark as unseen"): i18n("Mark as seen")
                    onTriggered: model.bug.seen = !model.bug.seen
                }
            ]
        }
    }

    Kirigami.OverlaySheet {
        id: filterSheet

        header: Kirigami.Heading {
            text: i18n("Filter bugs")
        }

        contentItem: Kirigami.FormLayout {
            // “Flags” and “Severity” “Os” “Reporter”, “Version”.
            Controls.ComboBox {
                id: seenCombo
                model: ListModel {
                    ListElement { text: "All"; data: 0}
                    ListElement { text: "Only Unseen"; data: 1; }
                    ListElement { text: "Only Seen"; data: 2}
                }
                textRole: "text"
                valueRole: "data"
                currentIndex: proxyBugsModel.seenFilter
                Layout.fillWidth: true
                Kirigami.FormData.label: i18n("Seen Status:")
            }
            Controls.ComboBox {
                id: severityCombo
                model: ListModel {
                    ListElement { text: "All"; data: ""}
                    ListElement { text: "Wishlist"; data: "wishlist"; }
                    ListElement { text: "Normal"; data: "normal"; }
                    ListElement { text: "Critical"; data: "critical"; }
                    ListElement { text: "Grave"; data: "grave"; }
                    ListElement { text: "Crash"; data: "crash"; }
                    ListElement { text: "Minor"; data: "minor"; }
                    ListElement { text: "Task"; data: "task"; }
                }
                textRole: "text"
                valueRole: "data"
                Component.onCompleted: currentIndex = indexOfValue(proxyBugsModel.severityFilter)
                Layout.fillWidth: true
                Kirigami.FormData.label: i18n("Severity:")
            }
            Controls.ComboBox {
                id: statusCombo
                model: ListModel {
                    ListElement { text: "All"; data: ""}
                    ListElement { text: "Confirmed"; data: "confirmed"; }
                    ListElement { text: "Unconfirmed"; data: "unconfirmed"; }
                    ListElement { text: "Needs Info"; data: "needsinfo"; }
                    ListElement { text: "Reopened"; data: "reopened"; }
                }
                textRole: "text"
                valueRole: "data"
                Component.onCompleted: currentIndex = indexOfValue(proxyBugsModel.statusFilter)
                Layout.fillWidth: true
                Kirigami.FormData.label: i18n("Status:")
            }
            Controls.ComboBox {
                id: osCombo
                model: ListModel {
                    ListElement { text: "All"; data: ""}
                    ListElement { text: "Unspecified"; data: "Unspecified"; }
                    ListElement { text: "Linux"; data: "Linux"; }
                    ListElement { text: "FreeBSD"; data: "FreeBSD"; }
                    ListElement { text: "Microsoft Windows"; data: "Microsoft Windows"; }
                    ListElement { text: "macOS"; data: "macOS"; }
                    ListElement { text: "Other"; data: "Other"; }
                }
                textRole: "text"
                valueRole: "data"
                Component.onCompleted: currentIndex = indexOfValue(proxyBugsModel.osFilter)
                Layout.fillWidth: true
                Kirigami.FormData.label: i18n("OS:")
            }
            Controls.TextField {
                id: versionField
                Layout.fillWidth: true
                Kirigami.FormData.label: i18n("Version:")
                text: proxyBugsModel.versionFilter
            }
            RowLayout {
                Kirigami.FormData.label: i18n("Flag:")
                Controls.ComboBox {
                    id: flagOperatorCombo
                    model: ListModel {
                        ListElement { text: "IS"; data: 0; }
                        ListElement { text: "NOT"; data: 1; }
                    }
                    textRole: "text"
                    valueRole: "data"
                    Component.onCompleted: currentIndex = indexOfValue(proxyBugsModel.flagsOperator)
                }
                Controls.ComboBox {
                    id: flagCombo
                    model: flagsArray
                    Layout.fillWidth: true
                }
            }
            Controls.TextField {
                id: searchField
                Layout.fillWidth: true
                Kirigami.FormData.label: i18n("Search:")
                text: proxyBugsModel.searchFilter
            }
        }

        footer: RowLayout {
            Item {
                Layout.fillWidth: true
            }

            Controls.ToolButton {
                text: i18n("OK")
                onClicked: {
                    proxyBugsModel.osFilter = osCombo.currentValue
                    proxyBugsModel.seenFilter = seenCombo.currentValue
                    proxyBugsModel.severityFilter = severityCombo.currentValue
                    proxyBugsModel.statusFilter = statusCombo.currentValue
                    proxyBugsModel.searchFilter = searchField.text
                    proxyBugsModel.versionFilter = versionField.text
                    proxyBugsModel.flagsFilter = flagCombo.currentText
                    proxyBugsModel.flagsOperator = flagOperatorCombo.currentValue
                    filterSheet.close()
                }
            }

            Controls.ToolButton {
                text: i18n("Cancel")
                onClicked: filterSheet.close()
            }
        }
    }

}

