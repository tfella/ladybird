// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: 2022 Julius Künzel <jk.kdedev@smartlab.uber.space>
#ifndef TEMPLATE_H
#define TEMPLATE_H

#include <QObject>

#include "bug.h"

class Template : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString id READ id CONSTANT)
    Q_PROPERTY(QString title READ title NOTIFY titleChanged)
    Q_PROPERTY(QString content READ content NOTIFY contentChanged)


public:
    explicit Template(const QString &id, const QString &title, const QString &content, QObject *parent = nullptr);

    QString id() const { return m_id; };
    QString title() const { return m_title; };
    QString content() const { return m_content; };

    Q_INVOKABLE QString expandedContent(Bug *bug);
    Q_INVOKABLE void update(const QString &title, const QString &content);

Q_SIGNALS:
    void titleChanged(const QString &title);
    void contentChanged(const QString &content);

private:
    QString m_id;
    QString m_title;
    QString m_content;
};

#endif // TEMPLATE_H
