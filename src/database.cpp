// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: 2022 Julius Künzel <jk.kdedev@smartlab.uber.space>

#include <QDir>
#include <QSqlDatabase>
#include <QSqlError>
#include <QStandardPaths>
#include <QDebug>

#include "database.h"

#define TRUE_OR_RETURN(x)                                                                                                                                      \
    if (!x)                                                                                                                                                    \
        return false;

Database::Database()
{
    QSqlDatabase db = QSqlDatabase::addDatabase(QStringLiteral("QSQLITE"));
    QString databasePath = path();
    QDir(databasePath).mkpath(databasePath);
    db.setDatabaseName(databasePath + QStringLiteral("/database.db3"));
    db.open();

    if (!init()) {
        qCritical() << "Failed to initialize the database";
    }
}

QString Database::path()
{
    return QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
}

bool Database::init()
{
    TRUE_OR_RETURN(execute(QStringLiteral("CREATE TABLE IF NOT EXISTS Bugs (id TEXT UNIQUE, seen bool);")));
    TRUE_OR_RETURN(execute(QStringLiteral("CREATE TABLE IF NOT EXISTS Templates (id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT, content TEXT);")));
    return true;
}

bool Database::execute(const QString &query)
{
    QSqlQuery q;
    q.prepare(query);
    return execute(q);
}

bool Database::execute(QSqlQuery &query)
{
    if (!query.exec()) {
        qWarning() << "Failed to execute SQL Query";
        qWarning() << query.lastQuery();
        qWarning() << query.lastError();
        return false;
    }
    return true;
}

void Database::setSeen(const QString &bugId, bool seen)
{
    QSqlQuery query;
    query.prepare(QStringLiteral("UPDATE Bugs SET seen=:seen WHERE id=:id"));
    query.bindValue(QStringLiteral(":id"), bugId);
    query.bindValue(QStringLiteral(":seen"), seen);
    execute(query);

    Q_EMIT bugSeenChanged(bugId, seen);
}

bool Database::updateTemplate(const QString &id, const QString &title, const QString &content)
{
    QSqlQuery query;
    query.prepare(QStringLiteral("UPDATE Templates SET title=:title, content=:content WHERE id=:id"));
    query.bindValue(QStringLiteral(":id"), id);
    query.bindValue(QStringLiteral(":title"), title);
    query.bindValue(QStringLiteral(":content"), content);
    return execute(query);
}

void Database::addTemplate(const QString &title, const QString &content)
{
    QSqlQuery query;
    query.prepare(QStringLiteral("INSERT INTO Templates (title, content) VALUES (:title,:content);"));

    query.bindValue(QStringLiteral(":title"), title);
    query.bindValue(QStringLiteral(":content"), content);
    execute(query);
    Q_EMIT templatesChanged();
}

void Database::deleteTemplate(const QString &id)
{
    QSqlQuery query;
    query.prepare(QStringLiteral("DELETE FROM Templates WHERE id = :id;"));
    query.bindValue(QStringLiteral(":id"), id);
    execute(query);

    Q_EMIT templatesChanged();
}
