// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: 2022 Julius Künzel <jk.kdedev@smartlab.uber.space>
#include "bug.h"

#include <QAbstractListModel>
#include <QStringList>

class BugModel : public QAbstractListModel
{
    Q_OBJECT

    Q_PROPERTY(bool refreshing READ refreshing WRITE setRefreshing NOTIFY refreshingChanged)
    Q_PROPERTY(QString errorString READ errorString NOTIFY errorStringChanged)

public:
    enum BugRoles {
        IdRole = Qt::UserRole + 1,
        StatusRole,
        SummaryRole,
        PlatformRole,
        OsRole,
        VersionRole
    };

    BugModel(QObject *parent = nullptr);


    void addBug(Bug *bug);

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    Q_INVOKABLE void fetch();

    bool refreshing() const { return m_refreshing; };
    void setRefreshing(bool refreshing);

    Q_INVOKABLE QStringList getFlagsForFilter();

    QString errorString() const { return m_errorString; };

protected:
    QHash<int, QByteArray> roleNames() const override;

Q_SIGNALS:
    void refreshingChanged(bool refreshing);
    void errorStringChanged(const QString &errorString);

private:
    QVector<Bug *> m_bugs;
    bool m_refreshing = false;
    QString m_errorString;

    void processResult(QByteArray &data);
};
